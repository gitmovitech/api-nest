import { Controller, Get, Request, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth/auth.service';
import { UsersService } from './users/users.service';

@Controller('api')
export class AppController {

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Post('signin')
  async signin(@Request() req) {
    let signin = false;
    if (req.body.phone && req.body.password) {
      signin = true;
    }
    if (req.body.email && req.body.password) {
      signin = true;
    }
    if (req.body.run && req.body.password) {
      signin = true;
    }
    if (signin) {
      return await this.authService.signin(req.body);
    } else {
      return {
        status: false,
        message: 'Usuario o contraseña incorrecta',
      };
    }
  }

  @Post('signup')
  async signup(@Request() req) {
    return this.usersService.create(req.body);
  }

  @Post('phone-code')
  async phoneCode(@Request() req) {
    return this.authService.phoneCode(req.body);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  getProfile(@Request() req) {
    return req.user;
  }

}
