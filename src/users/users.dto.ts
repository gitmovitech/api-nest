export class UsersDTO {
    readonly phone: string;
    readonly firstname: string;
    readonly lastname: string;
    readonly email: string;
    readonly run: string;
    password: string;
}
