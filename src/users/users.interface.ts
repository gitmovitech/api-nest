import { Document } from 'mongoose';

export interface User extends Document {
    readonly _id: object;
    readonly phone: string;
    readonly firstname: string;
    readonly lastname: string;
    readonly email: string;
    readonly run: string;
}