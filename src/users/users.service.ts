import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './users.interface';
import { UsersDTO } from './users.dto';

@Injectable()
export class UsersService {

    constructor(@InjectModel('User') private readonly model: Model<User>) { }

    async findOne(query: any): Promise<User> {
        return await this.model.findOne(query).exec();
    }

    async create(createDto: UsersDTO): Promise<User> {
        createDto.password = this.randomPassword();
        const created = new this.model(createDto);
        return await created.save();
    }

    private randomPassword() {
        let password = Math.random().toString(36).slice(-8);
        password += Math.random().toString(36).slice(-8);
        password += Math.random().toString(36).slice(-8);
        password += Math.random().toString(36).slice(-8);
        password += Math.random().toString(36).slice(-8);
        return password;
    }

}
