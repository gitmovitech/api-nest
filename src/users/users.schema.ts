import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema(
    {
        phone: String,
        firstname: String,
        lastname: String,
        email: String,
        run: String,
        password: String,
    },
    {
        collection: 'User',
        read: 'nearest',
    },
);
