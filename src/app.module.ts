import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { LocationModule } from './location/location.module';
import { environment } from './environment/environment';
import { TaxstoreModule } from './modules/taxstore/taxstore.module';

@Module({
  imports: [MongooseModule.forRoot(environment.mongoString, {
    useNewUrlParser: true,
  }), AuthModule, UsersModule, LocationModule, TaxstoreModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
