import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ) { }

    async validateUser(data: string): Promise<any> {
        return await this.usersService.findOne(data);
    }

    async signin(data: any) {
        const response: User = await this.usersService.findOne(data);
        if (response) {
            const payload: User = {
                _id: response._id,
                run: response.run,
                email: response.email,
                firstname: response.firstname,
                lastname: response.lastname,
                phone: response.phone,
            };
            return {
                status: true,
                data: this.jwtService.sign(payload),
            };
        } else {
            return {
                status: false,
                message: 'Usuario o contraseña incorrecta',
            };
        }
    }

    async phoneCode(data: any) {
        return data;
    }
}
