export const environment = {
    production: false,
    port: 8080,
    host: '127.0.0.1',
    apiBaseUrl: 'http://localhost:8080',
    mongoString: 'mongodb://10.128.0.7:27017/taxstore',
    jwtSecret: '!C8#Cf_am=9TUU2u9Nnq8c8Ffhm9ELW4',
};
