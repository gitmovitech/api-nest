import { Document } from 'mongoose';

export interface Location extends Document {
    readonly _id: object;
    readonly lat: number;
    readonly lng: number;
    readonly timestamp: number;
    readonly user_id: string;
    readonly bearing?: number;
    readonly speed?: number;
}
