import * as mongoose from 'mongoose';

export const LocationSchema = new mongoose.Schema(
    {
        lat: Number,
        lng: Number,
        timestamp: Number,
        user_id: String,
        bearing: Number,
        speed: Number,
    },
    {
        collection: 'Location',
        read: 'nearest',
    },
);
