import { Module } from '@nestjs/common';
import { LocationService } from './location.service';
import { MongooseModule } from '@nestjs/mongoose';
import { LocationSchema } from './location.schema';
import { LocationController } from './location.controller';

@Module({
  imports: [MongooseModule.forFeature([{
    name: 'Location',
    schema: LocationSchema,
  }])],
  providers: [LocationService],
  controllers: [LocationController],
})
export class LocationModule { }
