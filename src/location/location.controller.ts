import { Controller, UseGuards, Post, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LocationService } from './location.service';

@Controller('api/location')
export class LocationController {

    constructor(private readonly service: LocationService) { }

    @UseGuards(AuthGuard('jwt'))
    @Post()
    async Create(@Request() req) {
        return await this.service.create(req.body);
    }

}
