export class LocationDTO {
    readonly lat: number;
    readonly lng: number;
    readonly timestamp: number;
    readonly user_id: string;
    readonly bearing: number;
    readonly speed: number;
}
