import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Location } from './location.interface';
import { LocationDTO } from './location.dto';

@Injectable()
export class LocationService {

  constructor(@InjectModel('Location') private readonly model: Model<Location>) { }

  async create(createDto: LocationDTO): Promise<Location> {
    const created = new this.model(createDto);
    return await created.save();
  }

}
